/*
 *Given an array of positive numbers,
 *find the maximum sum of a subsequence with the constraint that no 2 numbers in the sequence should be adjacent in the array.
 *So 3 2 7 10 should return 13 (sum of 3 and 10) or 3 2 5 10 7 should return 15 (sum of 3, 5 and 7).
 *Answer the question in most efficient way.
 *Time=O(n) space O(n)
 *space can be further optimized as only previous two values are required to calculate current value and last value is returned
 *So optimized space will take O(1)
 *sum(0,i)=max(sum(0,i-2)+array[i],sum(0,i-1))
 */
#include <cstdio>
#define IN(a) scanf("%d",&a)
#define FOR(a,b,c) for(a=c;a<b;a++)
using namespace std;
/* returns maximum of two numbers */
int max(int a,int b){
	if(b>a)
		return b;
	return a;
}
int maxSum(int *array,int n){
	int i;	//loop variable
	int *sum=new int[n];	//this array will store sum from index 0 to index i
	sum[0]=array[0];
	sum[1]=max(sum[0],array[1]);
	FOR(i,n,2){
		sum[i]=max(array[i]+sum[i-2],sum[i-1]);	
		sum[i]=max(sum[i],array[i]);	//if array contains negative element than array[i] is also to be considered
	}
	return sum[n-1];
}
int main(){
	int i;	// loop variable
	int n;	// no of elements in given array
	IN(n);
	int *array=new int[n];
	FOR(i,n,0)
		IN(array[i]);
	printf("%d\n",maxSum(array,n));
}
